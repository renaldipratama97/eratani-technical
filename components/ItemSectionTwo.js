import Image from "next/image";

export default function ItemSectionTwo({ image, number, string }) {
  return (
    <div className="flex flex-col justify-center items-center w-6/12">
      <div className="w-20 h-20 md:w-32 md:h-32">
        <Image
          sizes="100vw"
          align="center"
          width="100%"
          height="100%"
          alt="farmers-image"
          src={`/images/${image}`}
        />
      </div>
      <div className="flex flex-col justify-center items-center mt-5">
        <h6 className="text-green font-extrabold text-xs sm:text-base md:text-xl">
          {number}
        </h6>
        <p className="text-yellow text-sm md:text-lg font-normal text-center">
          {string}
        </p>
      </div>
    </div>
  );
}
