import Image from "next/image";
import { Card } from "antd";
const { Meta } = Card;

export default function CardComponent(props) {
  return (
    <div className="mt-5 mx-auto">
      <Card
        key={props.key}
        hoverable
        style={{ width: 240 }}
        cover={
          <Image
            sizes="100vw"
            align="center"
            width="100%"
            height="100%"
            alt="farmers-image"
            src={props.image}
          />
        }
      >
        <Meta title={props.title} description={props.description} />
      </Card>
    </div>
  );
}
