import Link from "next/link";

export default function Button({ title, href }) {
  return (
    <Link href={href} passHref>
      <button className="h-9 w-44 bg-green font-semibold hover:bg-yellow hover:text-text rounded-lg text-white shadow-sm transition duration-200 ease-in-out transform hover:scale-110 hover:z-50">
        {title}
      </button>
    </Link>
  );
}
