import { useRef } from "react";
import { Carousel } from "antd";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faChevronRight,
  faChevronLeft,
} from "@fortawesome/free-solid-svg-icons";

export default function SectionOne() {
  const carousel = useRef();

  const handleNext = () => {
    carousel.current.next();
  };

  const handlePrev = () => {
    carousel.current.prev();
  };

  return (
    <div className="section__one">
      <Carousel autoplay effect="fade" ref={carousel}>
        <div className="sliderOne">
          <div className="w-full h-full bg-black bg-opacity-50 flex justify-center items-center gap-3">
            <div
              className="w-2/12 h-full flex justify-center items-center cursor-pointer"
              onClick={handlePrev}
            >
              <a
                onClick={handlePrev}
                className="w-full h-36 cursor-pointer justify-center items-center flex bg-transparent hover:text-white text-gray-300 rounded-full rounde"
              >
                <FontAwesomeIcon icon={faChevronLeft} className="w-16 h-16" />
              </a>
            </div>
            <div className="w-7/12 h-full flex flex-col justify-center items-center">
              <h3 className="text-gray-300 text-3xl md:text-4xl lg:text-5xl font-bold text-center">
                #SelaluAdaUntukPetani
              </h3>
              <p className="text-center text-gray-300 font-medium text-md md:text-lg lg:text-xl">
                Eratani adalah perusahaan startup Agri-tech yang fokus membangun
                sebuah ekosistem pertanian yang kuat dengan mendigitalisasi
                proses pertanian dari hulu hingga ke hilir. Eratani berupaya
                memberikan kemudahan akses kepada petani melalui teknologi yang
                kami miliki untuk meningkatkan produktivitas dan kesejahteraan
                ekosistem pertanian.
              </p>
            </div>
            <div
              className="w-2/12 h-full flex justify-center items-center cursor-pointer"
              onClick={handleNext}
            >
              <a
                onClick={handleNext}
                className="w-full h-36 cursor-pointer justify-center items-center flex bg-transparent hover:text-white text-gray-300 rounded-full rounde"
              >
                <FontAwesomeIcon icon={faChevronRight} className="w-16 h-16" />
              </a>
            </div>
          </div>
        </div>
        <div className="sliderTwo">
          <div className="w-full h-full bg-black bg-opacity-50 flex justify-center items-center gap-3">
            <div
              onClick={handlePrev}
              className="w-2/12 h-full flex justify-center items-center cursor-pointer"
            >
              <a
                onClick={handlePrev}
                className="w-full h-36 cursor-pointer justify-center items-center flex bg-transparent hover:text-white text-gray-300 rounded-full rounde"
              >
                <FontAwesomeIcon icon={faChevronLeft} className="w-16 h-16" />
              </a>
            </div>
            <div className="w-7/12 h-full flex flex-col justify-center items-center">
              <h3 className="text-gray-300 text-3xl md:text-4xl lg:text-5xl font-bold text-center">
                Bergerak Dari Hulu ke Hilir
              </h3>
              <p className="text-center text-gray-300 font-medium text-md md:text-lg lg:text-xl">
                Berbekal teknologi, Eratani fokus mendigitalisasi ekosistem
                pertanian dari hulu, termasuk pendanaan pertanian dan
                pengelolaan rantai pasokan. Hingga hilir, membantu proses
                distribusi dan penyaluran hasil panen.
              </p>
            </div>
            <div
              onClick={handleNext}
              className="w-2/12 h-full flex justify-center items-center cursor-pointer"
            >
              <a
                onClick={handleNext}
                className="w-full h-36 cursor-pointer justify-center items-center flex bg-transparent hover:text-white text-gray-300 rounded-full rounde"
              >
                <FontAwesomeIcon icon={faChevronRight} className="w-16 h-16" />
              </a>
            </div>
          </div>
        </div>
        <div className="sliderThree">
          <div className="w-full h-full bg-black bg-opacity-50 flex justify-center items-center gap-3">
            <div
              onClick={handlePrev}
              className="w-2/12 h-full flex justify-center items-center cursor-pointer"
            >
              <a
                onClick={handlePrev}
                className="cursor-pointerw-full h-36 bg justify-center items-center flex bg-transparent hover:text-white text-gray-300 rounded-full rounde"
              >
                <FontAwesomeIcon icon={faChevronLeft} className="w-16 h-16" />
              </a>
            </div>
            <div className="w-7/12 h-full flex flex-col justify-center items-center">
              <h3 className="text-gray-300 text-3xl md:text-4xl lg:text-5xl font-bold text-center">
                Menyejahterakan Petani Indonesia
              </h3>
              <p className="text-center text-gray-300 font-medium text-md md:text-lg lg:text-xl">
                Banyak petani Indonesia yang mengalami kesulitan untuk bercocok
                tanam karena terhalang oleh modal. Eratani hadir untuk
                memberikan bantuan finansial agar mereka tetap bisa bercocok
                tanam tanpa adanya hambatan sekaligus meningkatkan kesejahteraan
                hidup mereka.
              </p>
            </div>
            <div
              onClick={handleNext}
              className="w-2/12 h-full flex justify-center items-center cursor-pointer"
            >
              <a
                onClick={handleNext}
                className="w-full h-36 bg cursor-pointer justify-center items-center flex bg-transparent hover:text-white text-gray-300 rounded-full rounde"
              >
                <FontAwesomeIcon icon={faChevronRight} className="w-16 h-16" />
              </a>
            </div>
          </div>
        </div>
      </Carousel>
    </div>
  );
}
