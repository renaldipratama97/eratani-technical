import ItemSectionTwo from "./ItemSectionTwo";
import ItemsSectionTwo from "./ItemsSectionTwo";

export default function SectionTwo() {
  return (
    <div className="section__two h-auto w-full mt-10">
      <div className="mx-auto w-10/12 h-full">
        <h3
          style={{ lineHeight: "40px" }}
          className="font-semibold text-xl sm:text-2xl md:text-title-section text-green text-center"
        >
          Menuju Ekosistem yang <br /> Lebih Kuat Bersama Eratani
        </h3>
        <div className="flex w-8/12 mx-auto gap-3">
          <ItemSectionTwo
            image="petani-binaan.png"
            number="500+"
            string="Petani Binaan Eratani"
          />
          <ItemSectionTwo
            image="dana-tersalurkan.png"
            number="> Rp 10 Milyar"
            string="Pendanaan Tersalurkan"
          />
        </div>
        <div className="flex w-11/12 mx-auto gap-3">
          <ItemsSectionTwo
            image="peningkatan-pendapatan.png"
            number="> 15%"
            string="Peningkatan Pendapatan"
          />
          <ItemsSectionTwo
            image="luas-wilayah.png"
            number="1000 Ha+"
            string="Luas Wilayah Binaan"
          />
          <ItemsSectionTwo
            image="produktivitas.png"
            number="> 20%"
            string="Peningkatan Produktivitas"
          />
        </div>
      </div>
    </div>
  );
}
