import Image from "next/image";

export default function ItemSectionThree({ src, title, description }) {
  return (
    <div className="mx-auto w-10/12 mt-3">
      <div className="flex justify-center h-56 w-full bg-brown transition duration-200 ease-in-out transform hover:scale-105 hover:z-50">
        <div className="h-full w-3/12 flex justify-center items-center">
          <div className="w-52 h-52 ml-3 md:ml-0 flex justify-center items-center">
            <Image
              align="center"
              width={150}
              height={150}
              alt="image-pembiayaan"
              src={`/images/${src}`}
            />
          </div>
        </div>
        <div className="flex flex-col h-full w-9/12 justify-center ml-5 md:ml-0">
          <h3 className="text-green text-lg md:text-2xl font-medium">
            {title}
          </h3>
          <p className="text-text text-xs w-11/12 md:w-full md:text-base">
            {description}
          </p>
        </div>
      </div>
    </div>
  );
}
