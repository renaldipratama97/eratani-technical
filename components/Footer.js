import Image from "next/image";
import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faWhatsapp,
  faTiktok,
  faInstagram,
  faLinkedin,
  faYoutube,
} from "@fortawesome/free-brands-svg-icons";
import { faGlobe } from "@fortawesome/free-solid-svg-icons";

export default function Footer() {
  return (
    <footer className="this__footer w-full mt-16 flex flex-col">
      <div className="mx-auto flex flex-col w-10/12 mt-10">
        <div className="h-auto w-auto cursor-pointer">
          <Link href="/page-one" passHref>
            <Image
              src="/images/logo-white.png"
              width={165}
              height={50}
              alt="eratani-logo"
            />
          </Link>
        </div>
        <div className="flex flex-col md:flex-row justify-between gap-1 w-full md:h-52 mt-6">
          <div className="flex flex-col md:flex-row w-full md:w-8/12 h-full gap-3">
            <div className="footer__one flex flex-col w-full md:w-5/12 h-full">
              <p className="flex hover:text-yellow w-full md:w-56 h-auto text-sm font-normal leading-5 text-white cursor-pointer">
                Jl. Casablanca Raya Kav 88, Kel. Menteng Dalam, Kec. Tebet,
                Gedung Pakuwon Tower Lt 26 Unit J, Jakarta Selatan, DKI Jakarta
                12870, Indonesia
              </p>
              <span className="h-auto hover:text-yellow text-sm font-normal leading-5 text-white md:mt-auto cursor-pointer">
                Email : info.eratani@eratani.co.id
              </span>
              <span className="h-auto hover:text-yellow text-sm font-normal leading-5 text-white cursor-pointer">
                Telepon : +62 811 952 2577
              </span>
            </div>
            <div className="footer__two w-5/12 h-full flex flex-col mt-5 md:mt-0">
              <p className="h-auto text-2xl md:text-xl font-semibold leading-5 text-white cursor-pointer">
                Menu
              </p>
              <span className="h-auto hover:text-yellow text-sm font-normal leading-5 text-white cursor-pointer">
                Tim Kami
              </span>
              <span className="h-auto hover:text-yellow text-sm font-normal leading-5 text-white cursor-pointer">
                Mitra Eratani{" "}
              </span>
              <span className="h-auto hover:text-yellow text-sm font-normal leading-5 text-white cursor-pointer">
                Tips & Berita Pertanian Karir
              </span>
              <span className="h-auto hover:text-yellow text-sm font-normal leading-5 text-white cursor-pointer">
                Pertanian Karir
              </span>
            </div>
          </div>
          <div className="flex flex-col w-full md:w-3/12 h-full mt-5 md:mt-0">
            <div className="w-full text-white h-5 flex justify-center items-center">
              <span className="font-semibold">
                <FontAwesomeIcon
                  className="h-7 w-7 md:h-6 md:w-6 cursor-pointer"
                  icon={faGlobe}
                />
              </span>
              <span className="ml-3 font-semibold text-xl text-yellow cursor-pointer">
                ID
              </span>
              <span className="ml-3 font-semibold text-xl hover:text-yellow cursor-pointer">
                EN
              </span>
            </div>
            <div className="mt-10 md:mt-auto w-full h-auto flex justify-between items-center text-white">
              <FontAwesomeIcon
                className="h-7 w-7 md:h-5 md:w-5 hover:text-yellow cursor-pointer"
                icon={faTiktok}
              />
              <FontAwesomeIcon
                className="h-7 w-7 md:h-5 md:w-5 hover:text-yellow cursor-pointer"
                icon={faInstagram}
              />
              <FontAwesomeIcon
                className="h-7 w-7 md:h-5 md:w-5 hover:text-yellow cursor-pointer"
                icon={faLinkedin}
              />
              <FontAwesomeIcon
                className="h-7 w-7 md:h-5 md:w-5 hover:text-yellow cursor-pointer"
                icon={faYoutube}
              />
              <FontAwesomeIcon
                className="h-7 w-7 md:h-5 md:w-5 cursor-pointer hover:text-yellow"
                icon={faWhatsapp}
              />
            </div>
          </div>
        </div>
        <div className="copyright w-auto text-center flex justify-center text-sm font-normal items-center mt-5 text-white cursor-pointer">
          Copyright © 2021 by PT Eratani Teknologi Nusantara
        </div>
      </div>
    </footer>
  );
}
