import Image from "next/image";
import { HamburgerButton } from "react-hamburger-button";
import { useState } from "react";

export default function Navbar() {
  const [open, setOpen] = useState(false);

  const handleOpen = () => {
    setOpen(!open);
  };
  return (
    <nav className="w-full h-16 shadow-lg bg-white fixed z-50">
      <div className="mx-auto w-10/12 h-full flex justify-between items-center">
        <div className="images w-44 h-11 cursor-pointer">
          <Image
            sizes="100vw"
            align="center"
            width={176}
            height={44}
            alt="farmers-image"
            src="/images/logo_eratani.png"
          />
        </div>
        <span className="text-text font-semibold text-lg hover:text-green cursor-pointer hidden lg:inline-block">
          Beranda
        </span>
        <span className="text-text font-normal text-lg hover:text-green cursor-pointer hidden lg:inline-block">
          Tentang Kami
        </span>
        <span className="text-text font-normal text-lg hover:text-green cursor-pointer hidden lg:inline-block">
          Tips & Berita Pertanian
        </span>
        <span className="text-text font-normal text-lg hover:text-green cursor-pointer hidden lg:inline-block">
          Kegiatan
        </span>
        <div className="lg:flex hidden justify-center items-center hover:bg-opacity-80 hover:bg-yellow cursor-pointer bg-yellow w-36 h-9 font-semibold text-lg text-text">
          Mitra Petani
        </div>
        <div className="flex lg:hidden cursor-pointer bg-white justify-center rounded-md items-center w-12 h-10 bg-opacity-40">
          <HamburgerButton
            open={open}
            onClick={() => handleOpen()}
            width={23}
            height={18}
            strokeWidth={3}
            color="black"
            animationDuration={0.5}
          />
        </div>
      </div>
      {open ? (
        <div className="w-10/12 mx-auto bg-white h-auto mt-3">
          <ul className="w-full h-full text-base md:text-lg text-gray-800 font-light">
            <li className="flex justify-center items-center border-b w-full h-16">
              Beranda
            </li>
            <li className="flex justify-center items-center border-b w-full h-16">
              Tentang Kami
            </li>
            <li className="flex justify-center items-center border-b w-full h-16">
              Tips & Berita Pertanian
            </li>
            <li className="flex justify-center items-center border-b-2  w-full h-16">
              Kegiatan
            </li>
            <li className="flex justify-center items-center w-full h-16">
              <a className="bg-yellow text-gray-700 font-semibold text-xl w-44 h-12 flex justify-center items-center">
                Mitra Petani
              </a>
            </li>
          </ul>
        </div>
      ) : (
        ""
      )}
    </nav>
  );
}
