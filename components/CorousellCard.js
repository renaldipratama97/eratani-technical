import Image from "next/image";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronRight, faPlay } from "@fortawesome/free-solid-svg-icons";

export default function CorousellCard({ handleNext, image, title }) {
  return (
    <div className="swiper w-full">
      <div className="w-full md:w-8/12 h-auto md:h-64 mx-auto flex justify-end items-center">
        <div className="bg-brown w-11/12 h-full mr-10 flex flex-col md:flex-row gap-3">
          <div className="h-auto flex justify-center items-center w-12/12 md:w-4/12 my-auto">
            <div className="w-full h-full">
              <Image
                sizes="100vw"
                align="center"
                width="100%"
                height="100%"
                alt="farmers-image"
                src={`/images/${image}`}
              />
            </div>
          </div>
          <div className="h-full w-12/12 md:w-8/12 flex flex-col">
            <p className="text-black md:mt-7 w-full text-2xl font-semibold md:ml-10 text-center sm:text-left">
              {title}
            </p>
            <span className="ml-3 sm:ml-10 text-black text-md font-normal">
              Harapan saya Eratani semakin meluas karena petani sangat amat
              dibantu dengan adanya Eratani. Terima kasih Eratani!
            </span>
            <span className="flex justify-center text-black text-md font-normal items-center cursor-pointer h-12 w-40 mt-5 mx-auto sm:ml-auto sm:mr-10 bg-yellow hover:bg-opacity-80 hover:bg-yellow">
              <FontAwesomeIcon icon={faPlay} className="w-5 h-5" /> &nbsp; Lihat
              Video
            </span>
          </div>
        </div>
        <span
          onClick={handleNext}
          className="w-12 h-12 bg-yellow hover:bg-opacity-80 hover:bg-yellow cursor-pointer rounded-3xl text-black flex justify-center items-center"
        >
          <FontAwesomeIcon icon={faChevronRight} className="w-7 h-7" />
        </span>
      </div>
    </div>
  );
}
