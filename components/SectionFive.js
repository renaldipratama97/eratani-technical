import Image from "next/image";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faWhatsapp } from "@fortawesome/free-brands-svg-icons";

export default function SectionFive() {
  return (
    <div className="section__five mt-5 md:mt-0">
      <div className="mx-auto w-10/12">
        <div className="title flex justify-center items-center">
          <h3 className="font-semibold text-xl md:text-title-section leading-10 text-green text-center">
            Mitra Kami
          </h3>
        </div>

        <div className="w-full h-auto flex justify-between items-center mt-2 md:mt-12">
          <div className="w-80 h-20 md:h-32">
            <Image
              align="center"
              width={320}
              height={128}
              alt="bulog"
              src="/images/bulog.png"
            />
          </div>
          <div className="w-80 h-20 md:h-32">
            <Image
              align="center"
              width={320}
              height={128}
              alt="pangan"
              src="/images/pi_pangan.png"
            />
          </div>
          <div className="w-80 h-20 md:h-32">
            <Image
              align="center"
              width={320}
              height={128}
              alt="mdbt"
              src="/images/mdbt.png"
            />
          </div>
        </div>
        <div className="md:mt-16">
          <h3 className="font-semibold text-xl md:text-title-section leading-10 text-green text-center">
            Ingin Menjadi Bagian dari Kami?
          </h3>
          <p className="font-medium text-base md:text-lg lg:text-2xl text-text text-center">
            Bersama kami membangun pertanian Indonesia karena Eratani
            <span className="bg-yellow">
              {" "}
              <br /> &nbsp;#SelaluAdaUntukPetani
            </span>
          </p>
        </div>

        <div className="mt-5 md:mt-16 flex justify-center items-center">
          <span className="bg-green md:w-52 w-48 rounded-md h-10 md:h-12 text-md md:text-lg hover:bg-green-opacity flex justify-center items-center text-white">
            <FontAwesomeIcon
              className="w-5 h-5 md:w-7 md:h-7"
              icon={faWhatsapp}
            />
            &nbsp; Hubungi Kami
          </span>
        </div>
      </div>
    </div>
  );
}
