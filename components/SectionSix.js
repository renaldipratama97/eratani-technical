import { useEffect, useState, useRef } from "react";
import mapboxgl from "mapbox-gl/dist/mapbox-gl.js";

mapboxgl.accessToken =
  "pk.eyJ1IjoicmVuYWxkaXByYXRhbWE5NyIsImEiOiJja3d0OGVpaDYxYmhrMnZydWMwY3l5bXZsIn0.OqJNqjEkQFSCx_qiq1G3BQ";

export default function SectionSix() {
  const [lng, setLng] = useState(106.842609);
  const [lat, setLat] = useState(-6.22184);
  const [zoom, setZoom] = useState(15);

  const mapContainer = useRef(null);
  const map = useRef(null);

  useEffect(() => {
    if (map.current) return;
    map.current = new mapboxgl.Map({
      container: mapContainer.current,
      style: "mapbox://styles/mapbox/streets-v11",
      center: [lng, lat],
      zoom: zoom,
    });
  });

  return (
    <div className="section__six w-full md:mt-20">
      <div className="w-10/12 mx-auto flex flex-col lg:flex-row mt-10 gap-3">
        <div
          className="form__maps w-full lg:w-7/12 bg-green"
          ref={mapContainer}
        ></div>
        <div className="form__kritik w-full lg:w-5/12 bg-brown">
          <h3 className="w-full text-center mt-10 text-title-section font-medium text-text">
            Kritik & Saran
          </h3>

          <div className="mt-10 w-11/12 mx-auto">
            <form action="#" className="flex flex-col">
              <input
                type="text"
                placeholder="Name"
                className="text-text"
                autoComplete="off"
              />
              <input
                type="text"
                placeholder="Email"
                className="mt-5 text-text"
                autoComplete="off"
              />
              <input
                type="text"
                placeholder="Nomor Handphone"
                className="mt-5 text-text"
                autoComplete="off"
              />
              <input
                type="text"
                placeholder="Pesan Anda"
                className="mt-5 text-text"
                autoComplete="off"
              />
              <span className="mx-auto hover:bg-opacity-70 cursor-pointer hover:bg-yellow rounded-md text-lg font-semibold w-48 h-14 flex justify-center items-center mt-10 bg-yellow text-text">
                Kirim Pesan
              </span>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}
