import { useRef } from "react";
import { Carousel } from "antd";
import CorousellCard from "./CorousellCard";

export default function SectionFour() {
  const carousel = useRef();

  const handleNext = () => {
    carousel.current.next();
  };

  return (
    <div className="section__four w-full mt-20">
      <div className="w-10/12 mx-auto">
        <p className="font-semibold text-xl md:text-title-section leading-10 text-green text-center">
          Kata Mereka
        </p>
      </div>
      <div className="w-10/12 mx-auto">
        <Carousel autoplay effect="fade" ref={carousel} dots={false}>
          <CorousellCard
            handleNext={handleNext}
            image="warsoni.png"
            title="Warsoni | Petani"
          />
          <CorousellCard
            handleNext={handleNext}
            image="danawi.png"
            title="Danawi | Petani"
          />
          <CorousellCard
            handleNext={handleNext}
            image="daruna.png"
            title="Daruna | Petani"
          />
          <CorousellCard
            handleNext={handleNext}
            image="tarjaya.png"
            title="Tarjaya | Petani"
          />
        </Carousel>
      </div>
    </div>
  );
}
