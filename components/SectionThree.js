import ItemSectionThree from "./ItemSectionThree";

export default function SectionThree({ aos }) {
  return (
    <div className="section__three w-full" data-aos={aos}>
      <div className="flex items-center justify-center mt-10 w-10/12 mx-auto">
        <h3 className="font-semibold text-xl md:text-title-section leading-10 text-green text-center">
          Peduli Petani Bersama Eratani
        </h3>
      </div>

      <ItemSectionThree
        src="pembiayaan-container.png"
        title="Pembiayaan"
        description="Eratani menyalurkan dukungan dan edukasi finansial berbasis teknologi bagi para petani yang mengalami kesulitan permodalan untuk meningkatkan produktivitas pertanian."
      />
      <ItemSectionThree
        src="manajemen-container.png"
        title="Manajemen Rantai Pasok"
        description="Eratani memfasilitasi akses kebutuhan petani melalui mitra penyedia sarana kebutuhan di bidang pertanian secara transparan dan terstandarisasi."
      />
      <ItemSectionThree
        src="distribusi-container.png"
        title="Distribusi & Penjualan"
        description="Eratani memfasilitasi petani untuk menjual dan mendistribusikan hasil panen secara langsung dan mudah dengan harga yang terstandarisasi."
      />
    </div>
  );
}
