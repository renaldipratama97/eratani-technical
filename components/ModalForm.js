import { useEffect } from "react";
import { Modal, Form, Input } from "antd";

const ModalForm = ({ visible, onCreate, onUpdate, onCancel, title, store }) => {
  const [form] = Form.useForm();

  useEffect(() => {
    form.setFieldsValue({
      id: store?.id,
      fullname: store?.fullname,
      age: store?.age,
      gender: store?.gender,
      position: store?.position,
      address: store?.address,
    });
  }, [store]);

  const handleCancel = () => {
    form.resetFields();
    onCancel();
  };

  return (
    <Modal
      visible={visible}
      title={title}
      okText={title === "Add Data" ? "Create" : "Update"}
      cancelText="Cancel"
      onCancel={() => handleCancel()}
      onOk={() => {
        form
          .validateFields()
          .then((values) => {
            form.resetFields();
            if (title === "Add Data") {
              onCreate(values);
            } else {
              onUpdate(values);
            }
          })
          .catch((info) => {
            console.log("Validate Failed:", info);
          });
      }}
    >
      <Form
        form={form}
        layout="vertical"
        name="formModal"
        initialValues={{
          modifier: "public",
        }}
      >
        <Form.Item
          name="id"
          label="ID"
          rules={[
            {
              required: true,
              message: "Please input ID collection!",
            },
          ]}
        >
          <Input disabled={title === "Edit Data" ? true : false} />
        </Form.Item>
        <Form.Item
          name="fullname"
          label="Full Name"
          rules={[
            {
              required: true,
              message: "Please input Full Name!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="age"
          label="Age"
          rules={[
            {
              required: true,
              message: "Please input Age!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="gender"
          label="Gender"
          rules={[
            {
              required: true,
              message: "Please input Gender!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="position"
          label="Position"
          rules={[
            {
              required: true,
              message: "Please input Position!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="address"
          label="Address"
          rules={[
            {
              required: true,
              message: "Please input Address!",
            },
          ]}
        >
          <Input />
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default ModalForm;
