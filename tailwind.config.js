module.exports = {
  mode: "jit",
  purge: ["./pages/**/*.{js,ts,jsx,tsx}", "./components/**/*.{js,ts,jsx,tsx}"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      backgroundColor: {
        green: "#0e9749",
        yellow: "#FFCD00",
        "green-opacity": "#50c978",
      },
      colors: {
        green: "#0e9749",
        brown: "#F3F3F3",
        yellow: "#FFCD00",
        text: "#4d4d4d",
      },
      fontSize: {
        "title-section": "34px",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
