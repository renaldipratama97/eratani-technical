module.exports = {
  reactStrictMode: true,
  images: {
    domains: [
      "eratani.s3.ap-southeast-1.amazonaws.com",
      "os.alipayobjects.com",
    ],
  },
};
