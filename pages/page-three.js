import Head from "next/head";
import Link from "next/link";
import { Button } from "antd";
import Card from "../components/Card";
import axios from "axios";

export const getStaticProps = async () => {
  const getFarmers = await axios.post(
    `https://api.eratani.co.id/getAboutFarmer`
  );

  return {
    props: {
      data: getFarmers.data.data,
    },
  };
};

function PageThree({ data }) {
  return (
    <div className="h-auto min-h-screen w-full bg-green flex">
      <Head>
        <title>Eratani</title>
        <meta name="description" content="This project tes Eratani" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className="flex flex-col h-auto mx-auto w-full bg-white">
        <div className="w-11/12 flex justify-center items-center mx-auto">
          <Link href="/" passHref>
            <Button
              className="mt-5"
              type="default"
              shape="round"
              primary
              size="default"
            >
              Back to Home
            </Button>
          </Link>
        </div>
        <div className="w-11/12 mx-auto h-auto flex flex-wrap justify-between items-center">
          {data.map((item) => (
            <Card
              key={item.id}
              image={item.image}
              title={item.title}
              description={item.created_at}
            />
          ))}
        </div>
      </div>
    </div>
  );
}

export default PageThree;
