import "../styles/globals.css";
import "antd/dist/antd.css";
import "aos/dist/aos.css";

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />;
}

export default MyApp;
