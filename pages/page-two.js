import Head from "next/head";
import Link from "next/link";
import { Menu, Dropdown, Table, Button } from "antd";
import { useEffect, useState } from "react";
import datajson from "../utils/data.json";
import ModalForm from "../components/ModalForm";

export default function PageTwo() {
  const [state, setState] = useState([]);
  const [visible, setVisible] = useState(false);
  const [title, setTitle] = useState("");
  const [store, setStore] = useState("");
  const menu = (record) => {
    return (
      <Menu>
        <Menu.Item key="0">
          <a onClick={() => handleEdit(record)}>Edit Data</a>
        </Menu.Item>
        <Menu.Item key="1">
          <a onClick={() => handleDelete(record)}>Delete Data</a>
        </Menu.Item>
      </Menu>
    );
  };

  const handleEdit = (record) => {
    setStore(
      state.filter((el) => {
        if (el.id === record.id) {
          return el;
        }
      })[0]
    );
    setTitle("Edit Data");
    setVisible(true);
  };

  const handleDelete = (record) => {
    setState(state.filter((el) => el.id !== record.id));
  };

  const handleAddData = () => {
    setVisible(true);
    setTitle("Add Data");
  };

  const onCreate = (values) => {
    setState([...state, values]);
    setVisible(false);
  };

  const onUpdate = (values) => {
    setState((prev) => {
      return prev.map((item) => {
        if (item.id === values.id) {
          return values;
        } else {
          return item;
        }
      });
    });
    setVisible(false);
  };

  useEffect(() => {
    setState(datajson);
  }, []);

  const columns = [
    {
      title: "Full Name",
      dataIndex: "fullname",
      key: "fullname",
    },
    {
      title: "Age",
      dataIndex: "age",
      key: "age",
    },
    {
      title: "Gender",
      dataIndex: "gender",
      key: "age",
    },
    {
      title: "Position",
      dataIndex: "position",
      key: "position",
    },
    {
      title: "Address",
      dataIndex: "address",
      key: "address",
    },
    {
      title: "Action",
      key: "id",
      align: "center",
      fixed: "right",
      render: (record) => (
        <div className="flex flex-row justify-center items-center">
          <Dropdown overlay={() => menu(record)} trigger={["click"]}>
            <Button type="default" danger>
              . . .
            </Button>
          </Dropdown>
        </div>
      ),
    },
  ];

  return (
    <div className="bg-blue-200 h-auto min-h-screen">
      <Head>
        <title>Eratani</title>
        <meta name="description" content="This project tes Eratani" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className="w-full h-auto flex flex-col justify-center items-center">
        <div className="w-full lg:w-10/12 mt-5 flex items-center">
          <Link href="/" passHref>
            <Button type="default" shape="round" primary>
              Back to Home
            </Button>
          </Link>
          <Button
            className="ml-5"
            type="default"
            shape="round"
            primary
            onClick={() => handleAddData()}
          >
            Add Data
          </Button>
        </div>
        <div className="grid grid-cols-1 mt-5 w-full lg:w-10/12">
          <Table
            columns={columns}
            dataSource={state}
            scroll={{
              x: "600px",
            }}
          />
        </div>
      </div>

      <ModalForm
        title={title}
        store={store}
        visible={visible}
        onCreate={onCreate}
        onUpdate={onUpdate}
        onCancel={() => {
          setVisible(false);
        }}
      />
    </div>
  );
}
